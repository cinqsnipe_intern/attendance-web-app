<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    //
    protected $fillable = [
        'id','emp_name', 'emp_phone', 'emp_address'
    ];
}
