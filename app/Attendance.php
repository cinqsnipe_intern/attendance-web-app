<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    //
    Protected $fillable=[
   'id','check_in','status','check_out'
    ];
}
