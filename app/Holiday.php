<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
     protected $fillable = [
        'id','date','occasion'
    ];

   public function getDates()
   {
   	return ['date'];
   }
   public function setDate($value)
   {
   	$this->attributes['date']=Carbon::createFormFormat('d/m/Y H:i:s,$value');
   }
}
