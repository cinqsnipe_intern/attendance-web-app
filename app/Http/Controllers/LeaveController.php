<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Leave;

class LeaveController extends Controller
{
    private $leaves;

    public function __construct(Leave $Leave)
    {
        $this->leaves = $Leave;
    }
    //
     public function index()
    {
        //
        // get all the leaves
        $leaves = Leave::all();

        // load the view and pass the nerds
        return view('leaves.index',compact('leaves'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //

        return view('leaves.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */

    public function store(request $request)
    {
      // Leave::create($request->all());
      // $leaves=Leave::all();
      // return view('leaves.index',compact('leaves'));
      Leave::create($request->all());
        $leaves=Leave::all();
        return view('leaves.index',compact('leaves'));

      
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //

         $leaves = $this->leaves->find($id);
            //check if you got the value or not

         
        
        return view('leaves.edit',compact('leaves'));
//         $leaves=Leave::findorfail($id);
//        return view('leaves.edit',compact('leaves'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $leaves = Leave::findorfail($id);
        $leaves->update($request->all());
        return redirect('leaves');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $leaves=Leave::findorfail($id);
        $leaves->delete();
        return redirect('leaves');
    }

}

