<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingController extends Controller
{
    private $settings;

    public function __construct(Setting $Setting)
    {
        $this->settings = $Setting;
    }
    //
     public function index()
    {
        //
        // get all the settings
        $settings = Setting::all();

        // load the view and pass the nerds
        return view('settings.index',compact('settings'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //

        return view('settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */

    public function store(request $request)
    {
      // Setting::create($request->all());
      // $settings=Setting::all();
      // return view('settings.index',compact('settings'));
      Setting::create($request->all());
        $settings=Setting::all();
        return view('settings.index',compact('settings'));

      
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //

         $settings = $this->settings->find($id);
            //check if you got the value or not

         
        
        return view('settings.edit',compact('settings'));
//         $settings=Setting::findorfail($id);
//        return view('settings.edit',compact('settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $settings = Setting::findorfail($id);
        $settings->update($request->all());
        return redirect('settings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $settings=Setting::findorfail($id);
        $settings->delete();
        return redirect('settings');
    }

}

