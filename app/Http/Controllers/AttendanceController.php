<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attendance;

class AttendanceController extends Controller
{
    //
    private $attendances;

    public function __construct(Attendance $Attendance)
    {
        $this->attendances = $Attendance;

    }
     public function index()
    {
        //
        // get all the employees
        $attendances = Attendance::all();

        // load the view and pass the nerds
        return view('attendances.index',compact('attendances'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('attendances.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(request $request)
    {
        //
        Attendance::create($request->all());
        $attendances=Attendance::all();
        return view('attendances.index',compact('attendances'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $attendances = $this->attendances->find($id);
       
        dd(cha);
       
        // $attendances = $this->attendances->find($id);
        //     //check if you got the value or not

        
        
        // return view('attendances.show',compact('attendances'));
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $attendances = $this->attendances->find($id);
            //check if you got the value or not

        
        
        return view('attendances.edit',compact('attendances'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        //
         $attendances = Attendance::findorfail($id);
        $attendances->update($request->all());
        return redirect('attendances');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $attendances=Attendance::findorfail($id);
        $attendances->delete();
        return redirect('attendances');
    }

}

