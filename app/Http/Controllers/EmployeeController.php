<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\employee;

class EmployeeController extends Controller
{
    private $employees;

    public function __construct(employee $employee)
    {
        $this->employees = $employee;
    }
    //
     public function index()
    {
        //
        // get all the employees
        $employees = employee::all();

        // load the view and pass the nerds
        return view('employees.index',compact('employees'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //

        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */

    public function store(request $request)
    {
      // employee::create($request->all());
      // $employees=employee::all();
      // return view('employees.index',compact('employees'));
      employee::create($request->all());
        $employees=employee::all();
        return view('employees.index',compact('employees'));

      
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $data=employee::find($id);
        return view('employees.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //

         $employees = $this->employees->find($id);
            //check if you got the value or not

         
        
        return view('employees.edit',compact('employees'));
//         $employees=employee::findorfail($id);
//        return view('employees.edit',compact('employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $employees = employee::findorfail($id);
        $employees->update($request->all());
        return redirect('employees');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $employees=employee::findorfail($id);
        $employees->delete();
        return redirect('employees');
    }

}

