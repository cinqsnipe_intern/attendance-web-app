<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notice;
use DB;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function api_index()
     {
        $data=DB::table('notices')
        ->select('title', 'description',DB::raw('CONCAT("http://192.168.100.4:8080/images/", image) AS image'))
        ->get();
         
        return response()->json(array('noticejson'=>$data),200);
     }

    public function index()
    {
        $data=Notice::all();
        return view('notices.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('notices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       $temp=$request->all();
        if ($file=$request->file('image')){
            $name=$file->getClientOriginalName();
            $file->move('images',$name);
            $temp['image']=$name;
        }

  $data=[
              'title'=>$temp['title'],
              'description'=>$temp['description'],
              'image'=>$temp['image']
        ];
        Notice::create($data);
        $data=Notice::all();
        return view('notices.index',compact('data'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
