<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Auth;
use JWTFactory;

class AdminLoginController extends Controller
{
	public function __construct()
	{
		$this->middleware('guest');
	}

    public function authenticate(Request $request)
    {
       $credentials=$request->only('email','password');
       try{
        if(! $token=JWTAuth::attempt($credentials))
        {
            return $this->response->json()->errorUnauthorized();
        }
       }
       catch(JWTException $ex){
             
             return $this->response->array(compact('token'))->setStatusCode(200);
       }
    }
    public function ShowLoginForm()
    {
      return view('auth.adminlogin');
    }
    public function login(Request $request)
    {
    	//validating form
    	$this->validate($request,[
                'email'=>'required|email',
                'password'=>'required|min:6'
    		]);
    	//Attempt to log in Admin
    	if(auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember))
    	{
    		//if successful
    		return redirect()->intended(route('admin.dashboard'));
    	}
    	//if unsuccessful
    	return redirect()->back()->withInput($request->only('email','remember'));
    }
}
