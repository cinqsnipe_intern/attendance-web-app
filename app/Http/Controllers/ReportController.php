<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Report;

class ReportController extends Controller
{
    private $reports;

    public function __construct(Report $Report)
    {
        $this->reports = $Report;
    }
    //
     public function index()
    {
        //
        // get all the employees
        $reports = Report::all();

        // load the view and pass the nerds
        return view('reports.index',compact('reports'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //

        return view('reports.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */

    public function store(request $request)
    {
      // employee::create($request->all());
      // $employees=employee::all();
      // return view('employees.index',compact('employees'));
      Report::create($request->all());
        $reports=Report::all();
        return view('reports.index',compact('reports'));

      
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //

         $reports = $this->reports->find($id);
            //check if you got the value or not

         
        
        return view('reports.edit',compact('reports'));
//         $employees=employee::findorfail($id);
//        return view('employees.edit',compact('employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $reports = Report::findorfail($id);
        $reports->update($request->all());
        return redirect('reports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $reports=Report::findorfail($id);
        $reports->delete();
        return redirect('reports');
    }

}

