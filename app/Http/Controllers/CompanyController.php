<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\company;

class CompanyController extends Controller
{
    //
    private $companies;

    public function __construct(company $company)
    {
        $this->companies = $company;

    }
     public function index()
    {
        //
        // get all the employees
        $companies = company::all();

        // load the view and pass the nerds
        return view('companies.index',compact('companies'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(request $request)
    {
        //
        company::create($request->all());
        $companies=company::all();
        return view('companies.index',compact('companies'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $companies = $this->companies->find($id);
       
        dd(cha);
       
        // $companies = $this->companies->find($id);
        //     //check if you got the value or not

        
        
        // return view('companies.show',compact('companies'));
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $companies = $this->companies->find($id);
            //check if you got the value or not

        
        
        return view('companies.edit',compact('companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        //
         $companies = company::findorfail($id);
        $companies->update($request->all());
        return redirect('companies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $companies=company::findorfail($id);
        $companies->delete();
        return redirect('companies');
    }

}

