<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Holiday;
use DB;
use DateTime;
use Illuminate\Support\Facades\Input;

class HolidayController extends Controller
{
    private $holidays;

    public function __construct(Holiday $Holiday)
    {
        $this->holidays = $Holiday;
    }
    
  public function api_index()
{

    $holiday=DB::table('holidays')
        ->select('id','date','occasion')
        ->get();

    return response(['jsonholiday' => $holiday]);
}


     public function index()
    {
        //
        // get all the employees
        $holidays = Holiday::all();

        // load the view and pass the nerds
        return view('holidays.index',compact('holidays'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //

        return view('holidays.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */

    public function store(request $request)
    {
      // Holiday::create($request->all());
        $data=$request->all();
        // $date=DateTime::createFormFormat('d-m-Y H:i:s', Input::get('date'));
        // $usableDate=$date->format('Y-m-d H:i:s');
        $selectdata=[
                  'date'=>$data['date'],
                  'occasion'=>$data['occasion'],
        ];
        Holiday::create($selectdata);
        $holidays=Holiday::all();
        return view('holidays.index',compact('holidays'));

      
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //

         $holidays = $this->holidays->find($id);
            //check if you got the value or not

         
        
        return view('holidays.edit',compact('holidays'));
//         $employees=employee::findorfail($id);
//        return view('employees.edit',compact('employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $holidays = Holiday::findorfail($id);
        $holidays->update($request->all());
        return redirect('holidays');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $holidays=Holiday::findorfail($id);
        $holidays->delete();
        return redirect('holidays');
    }

}

