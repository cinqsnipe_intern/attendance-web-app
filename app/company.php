<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class company extends Model
{
    //
    protected $fillable = [
        'id','comp_name', 'comp_phone', 'comp_address'
    ];
}
