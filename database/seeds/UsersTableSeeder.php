<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user= new User;
        $user->employee_id='111';
        $user->email="momta@gmail.com";
        $user->username="momta";
        $user->password="momta1";
        $user->first_name="momta";
        $user->middle_name="";
        $user->last_name="shrestha";
        $user->role="admin";
        $user->dob="11/27/1994";
        $user->gender="male";
        $user->position="manager";
        $user->active="active";


        $user->save();
    }
}
