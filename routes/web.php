<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix'=>'admin'],function()
{
Route::get('/login','Auth\AdminLoginController@ShowLoginForm')->name('admin.login');
Route::post('/login','Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/','AdminController@index')->name('admin.dashboard');

});

Route::group(['middlewareGroup' => 'webGroup'],function(){
Auth::routes();

Route::group(['middleware'=>'auth'],function(){
Route::get('/home', 'HomeController@index');

Route::resource('/users','UserController');
Route::resource('/employees','EmployeeController');
Route::resource('/companies','CompanyController');
Route::resource('/holidays','HolidayController');
Route::resource('/reports','ReportController');
Route::resource('/settings','SettingController');
Route::resource('/leaves','LeaveController');
Route::resource('/attendances','AttendanceController');
Route::resource('/notices','NoticeController');
});
});
Route::any('jsontest','JsonController@jsonFunction');
Route::get('notification','JsonController@notification');
$api=app('Dingo\Api\Routing\Router');
$api->version('v1',function($api){
	$api->post('authenticate','Auth\AdminLoginController@authenticate');
	});
