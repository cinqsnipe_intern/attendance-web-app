
   

    {{--<form method="post" action="/holidays">--}}
    {{--{{csrf_field()}}--}}
    {{--<input type="text" name="title" placeholder="title">--}}
    {{--<input type="submit" name="submit">--}}
    {{--</form>--}}

@extends('blank')
@include('holidays.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('holidays') }}">Holidays Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('holidays') }}">View All Holidays</a></li>
        <li><a href="{{ URL::to('holidays/create') }}">Create a Holiday</a>
    </ul>
</nav>

<h1>Create a Holiday</h1>

<!-- if there are creation errors, they will show here -->
 <form class="form-horizontal" method="post" action="/holidays">
        {{csrf_field()}}
        <div>Add Holiday</div>
        <div class="form-group">
            <label for="id" class="col-sm-2 control-label">id</label>
            <div class="col-sm-8">
                <input type="id" class="form-control" id="id" placeholder="Holiday_id">
            </div>
        </div>

        <div class="form-group">
        <label for="date" class="col-sm-2 control-label">Date</label>
         <div class="col-sm-8">
             <input type="date" class="form-control" name="date" placeholder="Holiday-Date">
         </div>   
        </div>

        <div class="form-group">
            <label for="occasion" class="col-sm-2 control-label">Occasion</label>
            <div class="col-sm-8">
                <input required type="text" class="form-control" name="occasion" placeholder="Holiday Occasion">
            </div>
        </div>

        

        <div class="col-sm-offset-2 col-sm-10">
            <input required type="submit" name="submit" class="btn btn-primary">
        </div>

</form>
</div>
@endsection