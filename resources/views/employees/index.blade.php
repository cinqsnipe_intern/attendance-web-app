
<!-- app/views/nerds/index.blade.php -->

@extends('blank')
@include('employees.head')
@section('content')

<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('employees') }}">Employee Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('employees') }}">View All Employees</a></li>
        <li><a href="{{route('employees.create')}}">Create an Employee</a>
    </ul>
</nav>

<h1>All the Employees</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Phone</td>
            <td>Address</td>
            <td>Operation</td>
            
        </tr>
    </thead>
    <tbody>
    @foreach($employees as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->emp_name }}</td>
            <td>{{ $value->emp_phone }}</td>
            <td>{{ $value->emp_address }}</td>

            <!-- we will also add show, edit, and delete buttons -->
            <td>

                <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->

                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('employees/' . $value->id) }}">Show this Employee</a>

                <!-- edit this employee (uses the edit method found at GET /employee/{id}/edit -->

                <a class="btn btn-small btn-info" href="{{ route('employees.edit',$value->id) }}">Edit this Employee</a>
                

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

</div>
@endsection