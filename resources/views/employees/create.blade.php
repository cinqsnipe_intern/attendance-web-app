
 {{--
@section('title')
Employees Info
@endsection

@section('content')
<form class="form-horizontal" method="post" action="/employees">
  {{csrf_field()}}
    <div>Add Employee</div>
<div class="form-group">
    <label for="id" class="col-sm-2 control-label">id</label>
  <div class="col-sm-10">
      <input type="id" class="form-control" id="id" placeholder="Emp_id">
    </div>
  </div>



  <div class="form-group">
    <label for="emp_name" class="col-sm-2 control-label">Employee_name</label>
    <div class="col-sm-10">
      <input required type="text" class="form-control" name="emp_name" placeholder="Name">
    </div>
  </div>
      
  <div class="form-group">
    <label for="emp_phone" class="col-sm-2 control-label">Phone</label>
    <div class="col-sm-10">
      <input required type="string" class="form-control" name="emp_phone" placeholder="9800000000">
    </div>
  </div>
  <div class="form-group">
    <label for="emp_address" class="col-sm-2 control-label">Address</label>
    <div class="col-sm-10">
      <input required type="text" class="form-control" name="emp_address" placeholder="Address">
    </div>
  </div>
  

    <div class="col-sm-offset-2 col-sm-10">
      <input required type="submit" name="submit" class="btn btn-primary">
    </div>

</form> --}}

{{--<form method="post" action="/employees">--}}
  {{--{{csrf_field()}}--}}
  {{--<input type="text" name="title" placeholder="title">--}}
  {{--<input type="submit" name="submit">--}}
{{--</form>--}}


@extends('blank')
@include('employees.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('employees') }}">Employee Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('employees') }}">View All employees</a></li>
        <li><a href="{{ URL::to('employees/create') }}">Create an Employee</a>
    </ul>
</nav>

<h1>Create an Employee</h1>

<!-- if there are creation errors, they will show here -->
<form class="form-horizontal" method="post" action="/employees">
  {{csrf_field()}}
    <div>Add Employee</div>
<div class="form-group">
    <label for="id" class="col-sm-2 control-label">id</label>
  <div class="col-sm-10">
      <input type="id" class="form-control" id="id" placeholder="Emp_id">
    </div>
  </div>



  <div class="form-group">
    <label for="emp_name" class="col-sm-2 control-label">Employee_name</label>
    <div class="col-sm-10">
      <input required type="text" class="form-control" name="emp_name" placeholder="Name">
    </div>
  </div>
      
  <div class="form-group">
    <label for="emp_phone" class="col-sm-2 control-label">Phone</label>
    <div class="col-sm-10">
      <input required type="int" class="form-control" name="emp_phone" placeholder="9800000000">
    </div>
  </div>
  <div class="form-group">
    <label for="emp_address" class="col-sm-2 control-label">Address</label>
    <div class="col-sm-10">
      <input required type="text" class="form-control" name="emp_address" placeholder="Address">
    </div>
  </div>
  

    <div class="col-sm-offset-2 col-sm-10">
      <input required type="submit" name="submit" class="btn btn-primary">
    </div>

</form>
</div>
@endsection