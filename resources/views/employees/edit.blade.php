@extends('blank')
@include('employees.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('employees') }}">Employees Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('employees') }}">View All Employees</a></li>
        <li><a href="{{ URL::to('employees/create') }}">Create a Employee</a>
    </ul>
</nav>

<h1>Edit a Companies</h1>

    <form class="form-horizontal" method="post" action="/employees/{{$employees->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div>Edit</div>

        <div class="form-group">
            <label for="id" class="col-sm-2 control-label">id</label>
            <div class="col-sm-10">
                <input type="id" class="form-control" id="id" placeholder="employee_id">
            </div>
        </div>

       

        <div class="form-group">
            <label for="comp_name" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control"value="{{$employees->emp_name}}" name="emp_name" placeholder="employee name">
            </div>
        </div>
        <div class="form-group">
            <label for=comp_phone"" class="col-sm-2 control-label">Phone</label>
            <div class="col-sm-10">
                <input type="string" class="form-control"value="{{$employees->emp_phone}}" name="comp_phone" placeholder="9800000000">
            </div>
        </div>
        <div class="form-group">
            <label for="comp_address" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" value="{{$employees->emp_address}}" name="emp_address" placeholder="Employees address">
            </div>
        </div>

        <div class="col-sm-offset-2 col-sm-1">
                <input type="submit" name="submit" value="Update" class="btn btn-primary">
            </div>

    </form>

    <form method="post"  action="/employees/{{$employees->id}}">
        {{csrf_field()}}
    <input type="hidden" name="_method" value="DELETE">
    <input class="btn btn-danger" type="submit" value="Delete">
    </form>
   </div>
   @endsection
