@extends('blank')
@section('content')

<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('users') }}">Users Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('users') }}">View All Users</a></li>
        <li><a href="{{ URL::to('users/create') }}">Add a Users</a>
    </ul>
</nav>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
<h1> Users Info </h1>



    <table class="table-striped table-bordered table-condensed">
        <th>Employee_id</th>
        <th>Email</th>
        <th>Username</th>

        <th>role</th>
        <th>Date Of Birth</th>

        <th>Status</th>
        <th>Position</th>
        <th>Created At</th>
        <h2><a href="{{route('users.create')}}">Add Users</a></h2>

        @foreach($send as $a)
        <tr>
            <td>{{$a->employee_id}}</td>
            <td>{{$a->email}}</td>
            <td>{{$a->username}}</td>

           <td>{{$a->role}}</td>
            <td>{{$a->dob}}</td>

            <td>{{$a->active}}</td>
            <td>{{$a->position}}</td>
            <td>{{$a->created_at}}</td>
            <td><a href="{{route('users.edit',$a->id)}}"><button class="btn btn-primary">Edit</button></a></td>
            {{--<td><a href="{{route('users.edit',$a->id)}}"><button class="btn btn-danger">Delete</button></a></td>--}}

        </tr>
        @endforeach
    </table>
                </div>
            </div>
        </div>
    </div>
@endsection