@extends('blank')
@section('title')
    Add User
@endsection

@section('content')
    <form class="form-horizontal" method="post" action="/users/{{$user->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div>Edit</div>

        <div class="form-group">
            {{--<label for="id" class="col-sm-2 control-label">id</label>--}}
            {{--<div class="col-sm-8">
              <input type="id" class="form-control" id="id" placeholder="id">
            </div>
          </div>--}}

            <div class="form-group">
                <label for="employee_id" class="col-sm-2 control-label">Employee_id</label>
                <div class="col-sm-8">
                    <input  type="text" class="form-control disabled" name="employee_id" value="{{$user->employee_id}}" placeholder="Employee_id">
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-8">
                    <input type="email" class="form-control"value="{{$user->email}}" name="email" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <label for="username" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-8">
                    <input type="username" class="form-control" value="{{$user->username}}" name="username" placeholder="Username">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" value="{{$user->password}}" name="password" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <label for="first_name" class="col-sm-2 control-label">First_name</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$user->first_name}}" name="first_name" placeholder="first_name">
                </div>
            </div>
            <div class="form-group">
                <label for="middle_name" class="col-sm-2 control-label">Middle_name</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$user->middle_name}}" name="middle_name" placeholder="middle_name">
                </div>
            </div>
            <div class="form-group">
                <label for="last_name" class="col-sm-2 control-label">Last_name</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{$user->last_name}}" name="last_name" placeholder="last_name">
                </div>
            </div>
            <div class="form-group">
                <label for="gender" class="col-sm-2 control-label">Gender</label>
                <div class="col-sm-8">
                    <select name="gender" value="gender" class="form-control">
                        <option>Male</option>
                        <option>Female</option>

                    </select>
                </div>

            </div>
            <div class="form-group">
                <label for="dob" class="col-sm-2 control-label">Date of Birth</label>
                <div class="col-sm-8">
                    <input type="date" class="form-control" value="{{$user->dob}}"name="dob" placeholder="Date of Birth">
                </div>
            </div>


            <div class="form-group">
                <label for="position" class="col-sm-2 control-label">Position</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="position" value="{{$user->position}}" placeholder="Position">
                </div>
            </div>
            <div class="form-group">
                <label for="role" class="col-sm-2 control-label">Role</label>
                <div class="col-sm-8"><select name="role"value="{{$user->role}}" class="form-control">
                        <option>Admin</option>
                        <option>Employee</option>

                    </select>
                </div>

            </div>
            <div class="form-group">
                <label for="active" class="col-sm-2 control-label">Status</label>
                <div class="col-sm-8">
                    <select name="active" value="{{$user->active}}" class="form-control">
                        <option>Active</option>
                        <option>Inactive</option>

                    </select>
                </div>

            </div>



            <div class="col-sm-offset-2 col-sm-1">
                <input type="submit" name="submit" value="Update" class="btn btn-primary">
            </div>

    </form>

    <form method="post"  action="/users/{{$user->id}}">
        {{csrf_field()}}
    <input type="hidden" name="_method" value="DELETE">
    <input class="btn btn-danger" type="submit" value="Delete">
    </form>
@endsection