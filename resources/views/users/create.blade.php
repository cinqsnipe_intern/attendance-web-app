@extends('blank')
@section('title')
Add User 
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">

<form class="form-horizontal" method="post" action="/users">
  {{csrf_field()}}
	<div>Add</div>
<div class="form-group">
    {{--<label for="id" class="col-sm-2 control-label">id</label>--}}
    {{--<div class="col-sm-8">
      <input type="id" class="form-control" id="id" placeholder="id">
    </div>
  </div>--}}

  <div class="form-group">
    <label for="employee_id" class="col-sm-2 control-label">Employee_id</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="employee_id" placeholder="Employee_id">
    </div>
  </div>

  <div class="form-group">
    <label for="email" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-8">
      <input type="email" class="form-control" name="email" placeholder="Email">
    </div>
  </div>
  <div class="form-group">
    <label for="username" class="col-sm-2 control-label">Username</label>
    <div class="col-sm-8">
      <input type="username" class="form-control" name="username" placeholder="Username">
    </div>
  </div>
  <div class="form-group">
    <label for="password" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-8">
      <input type="password" class="form-control" name="password" placeholder="Password">
    </div>
  </div>
  {{--<div class="form-group">--}}
    {{--<label for="first_name" class="col-sm-2 control-label">First_name</label>--}}
    {{--<div class="col-sm-8">--}}
      {{--<input type="text" class="form-control" name="first_name" placeholder="first_name">--}}
    {{--</div>--}}
  {{--</div>--}}
   {{--<div class="form-group">--}}
    {{--<label for="middle_name" class="col-sm-2 control-label">Middle_name</label>--}}
    {{--<div class="col-sm-8">--}}
      {{--<input type="text" class="form-control" name="middle_name" placeholder="middle_name">--}}
    {{--</div>--}}
  {{--</div> --}}
  {{--<div class="form-group">--}}
    {{--<label for="last_name" class="col-sm-2 control-label">Last_name</label>--}}
    {{--<div class="col-sm-8">--}}
      {{--<input type="text" class="form-control" name="last_name" placeholder="last_name">--}}
    {{--</div>--}}
  {{--</div>--}}
 {{--<div class="form-group">--}}
    {{--<label for="gender" class="col-sm-2 control-label">Gender</label>--}}
    {{--<div class="col-sm-8">--}}
      {{--<select name="gender" class="form-control">--}}
  {{--<option>Male</option>--}}
  {{--<option>Female</option>--}}
  {{----}}
{{--</select>--}}
{{--</div>--}}
    {{----}}
  {{--</div>--}}
  {{--<div class="form-group">--}}
    {{--<label for="dob" class="col-sm-2 control-label">Date of Birth</label>--}}
    {{--<div class="col-sm-8">--}}
      {{--<input type="date" class="form-control" name="dob" placeholder="Date of Birth">--}}
    {{--</div>--}}
  {{--</div>--}}

  {{----}}
{{--<div class="form-group">--}}
    {{--<label for="position" class="col-sm-2 control-label">Position</label>--}}
    {{--<div class="col-sm-8">--}}
      {{--<input type="text" class="form-control" name="position" placeholder="Position">--}}
    {{--</div>--}}
  {{--</div>--}}
  {{--<div class="form-group">--}}
    {{--<label for="role" class="col-sm-2 control-label">Role</label>--}}
    {{--<div class="col-sm-8"><select name="role" class="form-control">--}}
  {{--<option>Admin</option>--}}
  {{--<option>Employee</option>--}}
  {{----}}
{{--</select>--}}
{{--</div>--}}
    {{----}}
  {{--</div>--}}
{{--<div class="form-group">--}}
    {{--<label for="active" class="col-sm-2 control-label">Status</label>--}}
    {{--<div class="col-sm-8">--}}
      {{--<select name="active" class="form-control">--}}
  {{--<option>Active</option>--}}
  {{--<option>Inactive</option>--}}
  {{----}}
{{--</select>--}}
{{--</div>--}}
    {{----}}
  {{--</div>--}}



    <div class="col-sm-offset-2 col-sm-8">
      <input type="submit" name="submit" class="btn btn-primary">
    </div>

</form>

{{--<form method="post" action="/users">--}}
  {{--{{csrf_field()}}--}}
  {{--<input type="text" name="title" placeholder="title">--}}
  {{--<input type="submit" name="submit">--}}
{{--</form>--}}
        </div>
      </div>
    </div>
  </div>
@endsection