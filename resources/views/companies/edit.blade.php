@extends('blank')
@include('companies.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('companies') }}">Comapny Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('companies') }}">View All Companies</a></li>
        <li><a href="{{ URL::to('companies/create') }}">Create a Company</a>
    </ul>
</nav>

<h1>Edit a Companies</h1>

    <form class="form-horizontal" method="post" action="/companies/{{$companies->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div>Edit</div>

        <div class="form-group">
            <label for="id" class="col-sm-2 control-label">id</label>
            <div class="col-sm-8">
                <input type="id" class="form-control"value="{{$companies->comp_name}}" id="id" placeholder="company_id">
            </div>
        </div>

       

        <div class="form-group">
            <label for="comp_name" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-8">
                <input type="text" class="form-control"value="{{$companies->comp_name}}" name="comp_name" placeholder="company name">
            </div>
        </div>
        <div class="form-group">
            <label for=comp_phone"" class="col-sm-2 control-label">Phone</label>
            <div class="col-sm-8">
                <input type="string" class="form-control"value="{{$companies->comp_phone}}" name="comp_phone" placeholder="9800000000">
            </div>
        </div>
        <div class="form-group">
            <label for="comp_address" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" value="{{$companies->comp_address}}" name="comp_address" placeholder="Company address">
            </div>
        </div>

        <div class="col-sm-offset-2 col-sm-1">
                <input type="submit" name="submit" value="Update" class="btn btn-primary">
            </div>

    </form>

    <form method="post"  action="/companies/{{$companies->id}}">
        {{csrf_field()}}
    <input type="hidden" name="_method" value="DELETE">
    <input class="btn btn-danger" type="submit" value="Delete">
    </form>
   </div>
   @endsection
