@extends('blank')
@include('employees.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('notices') }}">Notice Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('notices') }}">View All notices</a></li>
        <li><a href="{{ URL::to('notices/create') }}">Create a Notice</a>
    </ul>
</nav>

<h1>Create an Notice</h1>

<!-- if there are creation errors, they will show here -->
<form class="form-horizontal" enctype="multipart/form-data" method="post" action="/notices">
  {{csrf_field()}}
    <div>Add Notice</div>
<div class="form-group">
    <label for="id" class="col-sm-2 control-label">id</label>
  <div class="col-sm-8">
      <input type="id" class="form-control" id="id" placeholder="Notice_id">
    </div>
  </div>


  <div class="form-group">
        <label for="title" class="col-sm-2 control-label">Title</label>
         <div class="col-sm-8">
             <input type="text" class="form-control" name="title" placeholder="Notice Title">
         </div>   
        </div>
      
  <div class="form-group">
    <label for="description" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-8">
       <textarea rows="4" cols="50" name="description" placeholder="Description"></textarea>
    </div>
  </div>

   <div class="form-group">
        <label for="image" class="col-sm-2 control-label">image</label>
         <div class="col-sm-8">
             <input type="file" class="form-control" name="image" placeholder="image" multiple>
         </div>   
        </div>
  
    <div class="col-sm-offset-2 col-sm-8">
      <input required type="submit" name="submit" class="btn btn-primary">
    </div>

</form>
</div>
@endsection