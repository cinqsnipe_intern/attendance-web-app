<!-- app/views/nerds/index.blade.php -->
@extends('blank')
@include('attendances.head')
@section('content')

<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('attendances') }}">Attendance Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('attendances') }}">View All attendances</a></li>
        <li><a href="{{ URL::to('attendances/create') }}">Add a Attendance</a>
    </ul>
</nav>

<h1>All the Attendance</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>Status</td>
            <td>Checked In At</td>
            <td>Checked Out At</td>
            <td>Operation</td>
            
        </tr>
    </thead>
    <tbody>
    @foreach($attendances as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->status }}</td>
            <td>{{ $value->check_in}}</td>
            <td>{{ $value->check_out}}</td>

            <!-- we will also add show, edit, and delete buttons -->
            <td>

                <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->

                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('attendances/' . $value->id .'/show') }}">Show this Attendance</a>

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('attendances/' . $value->id . '/edit') }}">Edit this Attendance</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

</div>

@endsection