@extends('blank')
@include('attendances.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('attendances') }}">Attendance Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('attendances') }}">View All attendances</a></li>
        <li><a href="{{ URL::to('attendances/create') }}">Create a Company</a>
    </ul>
</nav>

<h1>Edit an attendances</h1>

    <form class="form-horizontal" method="post" action="/attendances/{{$attendances->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div>Edit</div>

        <div class="form-group">
            <label for="id" class="col-sm-2 control-label">id</label>
            <div class="col-sm-8">
                <input type="id" class="form-control"value="{{$attendances->id}}" id="id" placeholder="Attendance ID">
            </div>
        </div>

       

        <div class="form-group">
            <label for="check_in" class="col-sm-2 control-label">Checked In </label>
            <div class="col-sm-8">
                <input type="time" class="form-control"value="{{$attendances->id}}" name="check_in" placeholder="Check In Time">
            </div>
        </div>

          <div class="form-group">
    <label for="status" class="col-sm-2 control-label"value="{{$attendances->status}}">Attendance</label>
    <div class="col-sm-8">
<select name="status" class="form-control">
<option>Present</option>
<option>Absent</option>
<option>Out</option>
</select>
</div>
</div>

        <div class="form-group">
            <label for="comp_address" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" value="{{$attendances->comp_address}}" name="comp_address" placeholder="Company address">
            </div>
        </div>

        <div class="col-sm-offset-2 col-sm-1">
                <input type="submit" name="submit" value="Update" class="btn btn-primary">
            </div>

    </form>

    <form method="post"  action="/attendances/{{$attendances->id}}">
        {{csrf_field()}}
    <input type="hidden" name="_method" value="DELETE">
    <input class="btn btn-danger" type="submit" value="Delete">
    </form>
   </div>
   @endsection
