@extends('blank')
   

    {{--<form method="post" action="/employees">--}}
    {{--{{csrf_field()}}--}}
    {{--<input type="text" name="title" placeholder="title">--}}
    {{--<input type="submit" name="submit">--}}
    {{--</form>--}}

@include('companies.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('attendances') }}">attendances Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('attendances') }}">View All attendances</a></li>
        <li><a href="{{ URL::to('attendances/create') }}">Create a Attendance</a>
    </ul>
</nav>

<h1>Create an Attendance</h1>

<!-- if there are creation errors, they will show here -->
 <form class="form-horizontal" method="post" action="/attendances">
        {{csrf_field()}}
        <div>Add Attendance</div>
        <div class="form-group">
            <label for="id" class="col-sm-2 control-label">id</label>
            <div class="col-sm-10">
                <input type="id" class="form-control" id="id" placeholder="Attendance ID">
            </div>
        </div>


        <div class="form-group">
            <label for="check_in" class="col-sm-2 control-label">Checked In</label>
            <div class="col-sm-10">
                <input required type="time" class="form-control" name="check_in" placeholder="Checked In">
            </div>
        </div>

        <div class="form-group">
    <label for="status" class="col-sm-2 control-label">Attendance</label>
    <div class="col-sm-8">
<select name="status" class="form-control">
<option>Present</option>
<option>Absent</option>
<option>Out</option>
</select>
</div>
</div>
        <div class="form-group">
            <label for="check_out" class="col-sm-2 control-label">Checked Out</label>
            <div class="col-sm-10">
                <input required type="time" class="form-control" name="check_out" placeholder="Checked Out">
            </div>
        </div>


        <div class="col-sm-offset-2 col-sm-10">
            <input required type="submit" name="submit" class="btn btn-primary">
        </div>

</form>
</div>
@endsection