@extends('blank')

@section('content') 
<div class="table-responsive">   
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">   
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<table  class="table table-striped table-inverse" >
    <thead class="danger">
      <tr>
        <th>Name</th>
        <th>Department</th>
        <th>Phone</th>
        <th>Checked_in</th>
      </tr>
    </thead>
    <tbody>
    @php
use App\employee;
$data=employee::all();
    @endphp
    @foreach($data as $a)
      <tr>
        <td class="active">{{$a->emp_name}} </td>
        <td class="success">{{$a->emp_address}}</td>
        <td class="danger">{{$a->emp_phone}}</td>
        <td class="success">{{$a->created_at}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection

   