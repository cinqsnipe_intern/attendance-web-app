<!-- app/views/nerds/index.blade.php -->


@extends('blank')
@include('holidays.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('reports') }}">Company Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('reports') }}">View All Reports</a></li>
        <li><a href="{{ URL::to('reports/create') }}">Add a Report</a>
    </ul>
</nav>

<h1>All the Report</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>Report Title</td>
            <td>Report Description</td>
            <td>Operation</td>
            
        </tr>
    </thead>
    <tbody>
    @foreach($reports as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->title }}</td>
            <td>{{ $value->description }}</td>
                        <!-- we will also add show, edit, and delete buttons -->
            <td>

                <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->

                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('reports/' . $value->id .'/show')}}">Show this Report</a>

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('reports/' . $value->id . '/edit') }}">Edit this Report</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

</div>
@endsection