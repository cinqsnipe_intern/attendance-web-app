
   

    {{--<form method="post" action="/reports">--}}
    {{--{{csrf_field()}}--}}
    {{--<input type="text" name="title" placeholder="title">--}}
    {{--<input type="submit" name="submit">--}}
    {{--</form>--}}


@extends('blank')
@include('holidays.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('reports') }}">Report Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('reports') }}">View All Reports</a></li>
        <li><a href="{{ URL::to('reports/create') }}">Create a Report</a>
    </ul>
</nav>

<h1>Create a Report</h1>

<!-- if there are creation errors, they will show here -->
 <form class="form-horizontal" method="post" action="/reports">
        {{csrf_field()}}
        <div>Add Report</div>
        <div class="form-group">
            <label for="id" class="col-sm-2 control-label">id</label>
            <div class="col-sm-8">
                <input type="id" class="form-control" id="id" placeholder="Report ID">
            </div>
        </div>


        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Title</label>
            <div class="col-sm-8">
                <input required type="text" class="form-control" name="title" placeholder="Report Title">
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Report Description</label>
            <div class="col-sm-8">
                <input required type="string" class="form-control" name="description" placeholder="Report Description">
            </div>
        </div>
        
        <div class="col-sm-offset-2 col-sm-8">
            <input required type="submit" name="submit" class="btn btn-primary">
        </div>

</form>
</div>
@endsection