
@extends('blank')
@include('holidays.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('reports') }}">Comapny Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('reports') }}">View All reports</a></li>
        <li><a href="{{ URL::to('employees/create') }}">Create a Company</a>
    </ul>
</nav>

<h1>Edit a Report</h1>

    <form class="form-horizontal" method="post" action="/reports/{{$reports->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div>Edit</div>

        <div class="form-group">
            <label for="id" class="col-sm-2 control-label">id</label>
            <div class="col-sm-8">
                <input type="id" class="form-control" id="id" placeholder="Report ID">
            </div>
        </div>

       

        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Report Title</label>
            <div class="col-sm-8">
                <input type="text" class="form-control"value="{{$reports->title}}" name="title" placeholder="Report">
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Description</label>
            <div class="col-sm-8">
                <input type="string" class="form-control"value="{{$reports->description}}" name="description" placeholder="Report Description">
            </div>
        </div>
        
        <div class="col-sm-offset-2 col-sm-1">
                <input type="submit" name="submit" value="Update" class="btn btn-primary">
            </div>

    </form>

    <form method="post"  action="/reports/{{$reports->id}}">
        {{csrf_field()}}
    <input type="hidden" name="_method" value="DELETE">
    <input class="btn btn-danger" type="submit" value="Delete">
    </form>
   </div>
   @endsection 
