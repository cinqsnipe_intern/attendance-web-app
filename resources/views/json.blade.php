<!DOCTYPE html>
<html>
<head>
	<title>jsontest</title>
</head>
<body>
<div class="table-responsive">   
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">   
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<table  class="table table-striped table-inverse" >
    <thead class="danger">
      <tr>
        <th>ID</th>
        <th>Reason</th>
        <th>Description</th>
        <th>Created_at</th>
      </tr>
    </thead>
    <tbody>
    @foreach($data as $a)
      <tr>
        <td class="active">{{$a->id}} </td>
        <td class="success">{{$a->reason}}</td>
        <td class="danger">{{$a->description}}</td>
        <td class="success">{{$a->created_at}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
</body>
</html>