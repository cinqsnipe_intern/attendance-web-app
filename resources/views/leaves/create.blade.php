
   

    {{--<form method="post" action="/leaves">--}}
    {{--{{csrf_field()}}--}}
    {{--<input type="text" name="title" placeholder="title">--}}
    {{--<input type="submit" name="submit">--}}
    {{--</form>--}}


@extends('blank')
@include('holidays.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('leaves') }}">Leaves Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('leaves') }}">View All Leaves</a></li>
        <li><a href="{{ URL::to('leaves/create') }}">Create a Leave</a>
    </ul>
</nav>

<h1>Create a Company</h1>

<!-- if there are creation errors, they will show here -->
 <form class="form-horizontal" method="post" action="/leaves">
        {{csrf_field()}}
        <div>Add Company</div>
        <div class="form-group">
            <label for="id" class="col-sm-2 control-label">ID</label>
            <div class="col-sm-8">
                <input type="id" class="form-control" id="id" placeholder="Leave_id">
            </div>
        </div>


        <div class="form-group">
            <label for="reason" class="col-sm-2 control-label">Leave Reason</label>
            <div class="col-sm-8">
                <input required type="text" class="form-control" name="reason" placeholder="Leave Reason">
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Desription</label>
            <div class="col-sm-8">
                <input required type="text" class="form-control" name="description" placeholder="Description">
            </div>
            <div class="col-sm-offset-2 col-sm-10">
            <input required type="submit" name="submit" class="btn btn-primary">
        </div>


</form>
</div>
@endsection