
@extends('blank')
@include('leave.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('leaves') }}">Leave Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('leaves') }}">View All leaves</a></li>
        <li><a href="{{ URL::to('employees/create') }}">Create a Company</a>
    </ul>
</nav>

<h1>Edit a leaves</h1>

    <form class="form-horizontal" method="post" action="/leaves/{{$leaves->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div>Edit</div>

        <div class="form-group">
            <label for="id" class="col-sm-2 control-label">id</label>
            <div class="col-sm-10">
                <input type="id" class="form-control" id="id" placeholder="Leave id">
            </div>
        </div>

       

        <div class="form-group">
            <label for="reason" class="col-sm-2 control-label">Leave Reason</label>
            <div class="col-sm-10">
                <input type="text" class="form-control"value="{{$leaves->reason}}" name="reason" placeholder="company name">
            </div>
        </div>
        <div class="form-group">
            <label for=description"" class="col-sm-2 control-label">Leave Description</label>
            <div class="col-sm-10">
                <input type="string" class="form-control"value="{{$leaves->description}}" name="description" placeholder="9800000000">
            </div>
        </div>
        

        <div class="col-sm-offset-2 col-sm-1">
                <input type="submit" name="submit" value="Update" class="btn btn-primary">
            </div>

    </form>

    <form method="post"  action="/leaves/{{$leaves->id}}">
        {{csrf_field()}}
    <input type="hidden" name="_method" value="DELETE">
    <input class="btn btn-danger" type="submit" value="Delete">
    </form>
   </div>
   </body>
   </html> 
