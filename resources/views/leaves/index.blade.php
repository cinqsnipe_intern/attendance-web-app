<!-- app/views/nerds/index.blade.php -->


@extends('blank')
@include('holidays.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('leaves') }}">Leave Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('leaves') }}">View All leaves</a></li>
        <li><a href="{{ URL::to('leaves/create') }}">Add a Leave</a>
    </ul>
</nav>

<h1>All the Leave</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>Leave Reason</td>
            <td>Description</td>   
            <td>Operation</td>
            
        </tr>
    </thead>
    <tbody>
    @foreach($leaves as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->reason }}</td>
            <td>{{ $value->description }}</td>
            

            <!-- we will also add show, edit, and delete buttons -->
            <td>

                <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->

                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('leaves/' . $value->id .'/show')}}">Show this Leave</a>

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('leaves/' . $value->id . '/edit') }}">Edit this Leave</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>

</div>
@endsection