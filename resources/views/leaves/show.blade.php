<!DOCTYPE html>
<html>
<head>
    <title>Look! I'm CRUDding</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('companies') }}">Companies Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('nerds') }}">View All companies</a></li>
        <li><a href="{{ URL::to('nerds/create') }}">Create a Company</a>
    </ul>
</nav>

<h1>Showing {{ $companies->comp_name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $companies->comp_name }}</h2>
        <p>
            <strong>Address:</strong> {{ $companies->comp_address }}<br>
            <strong>Phone:</strong> {{ $companies->comp_phone }}
        </p>
    </div>

</div>
</body>
</html>