
   

    {{--<form method="post" action="/settings">--}}
    {{--{{csrf_field()}}--}}
    {{--<input type="text" name="title" placeholder="title">--}}
    {{--<input type="submit" name="submit">--}}
    {{--</form>--}}


@extends('blank')
@include('holidays.head')
@section('content')
<div class="container">

<nav class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ URL::to('settings') }}">Settings Alert</a>
    </div>
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('settings') }}">View All Settings</a></li>
        <li><a href="{{ URL::to('settings/create') }}">Create a Setting</a>
    </ul>
</nav>

<h1>Create a Setting</h1>

<!-- if there are creation errors, they will show here -->
 <form class="form-horizontal" method="post" action="/settings">
        {{csrf_field()}}
        <div>Add Setting</div>
        <div class="form-group">
            <label for="id" class="col-sm-2 control-label">id</label>
            <div class="col-sm-8">
                <input type="id" class="form-control" id="id" placeholder="Setting ID">
            </div>
        </div>


        <div class="form-group">
            <label for="comp_name" class="col-sm-2 control-label">comp_name</label>
            <div class="col-sm-8">
                <input required type="text" class="form-control" name="comp_name" placeholder="Setting Name">
            </div>
        </div>

        <div class="form-group">
            <label for="comp_phone" class="col-sm-2 control-label">Phone</label>
            <div class="col-sm-8">
                <input required type="string" class="form-control" name="comp_phone" placeholder="9800000000">
            </div>
        </div>
        <div class="form-group">
            <label for="comp_address" class="col-sm-2 control-label">Address</label>
            <div class="col-sm-8">
                <input required type="text" class="form-control" name="comp_address" placeholder="Address">
            </div>
        </div>


        <div class="col-sm-offset-2 col-sm-8">
            <input required type="submit" name="submit" class="btn btn-primary">
        </div>

</form>
</div>
@endsection